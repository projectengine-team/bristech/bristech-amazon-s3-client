# API Jupiter v2.0.0
# -------------------------------------------
# Modelo Dockerfile para utilizacao com container
#
# (conferir conexao com o banco antes do build)
# build da imagem:
# $ docker build -t sigtrans/jupiter-api:v2.0.0 .
#
# run padrao prd
# $ docker run -d --name s3client-api -e TZ=America/Sao_Paulo -v /var/data/:/var/data/ -p 8080:8080  --restart=always sigtrans/s3client-api:v2.0.0

# run padrao hmg
# $ docker run -d --name jupiter-apiHmo -e TZ=America/Sao_Paulo -v /var/data/:/var/data/ -p 8080:8080  --restart=always sigtrans/s3client-apiHmo:v2.0.0

FROM maven:3.6.3-openjdk-17

LABEL version="1.0"
LABEL description="Sigtrans Jupiter"
LABEL maintainer="Emilio Numazaki <emilio@bristech.com.br>"

#Variaveis
ARG CNT_S3_ROOT=/opt/sigtrans/mss/s3client-ms
ARG CNT_LOG_ROOT=/opt/sigtrans/logs

# Preparando ambiente de execução
RUN mkdir -p $CNT_LOG_ROOT
RUN mkdir -p $CNT_S3_ROOT
RUN chmod +x $CNT_S3_ROOT

WORKDIR $CNT_S3_ROOT

COPY ./docker_settings.xml $CNT_S3_ROOT

RUN mvn org.apache.maven.plugins:maven-dependency-plugin:get \
    "-DremoteRepositories=gitlab-maven::::https://gitlab.com/api/v4/groups/10851611/-/packages/maven"\
    "-Dartifact=br.com.bristech.amazon:s3-client-rt:2.0.0-SNAPSHOT" \
    "-Ddest=s3client-ms.jar" \
    -s docker_settings.xml

EXPOSE 8080

ENTRYPOINT ["java","-jar","s3client-ms.jar","--spring.profiles.active=prod"]

