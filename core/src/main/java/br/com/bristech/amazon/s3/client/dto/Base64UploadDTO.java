package br.com.bristech.amazon.s3.client.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@ApiModel(description = "File upload data")
public class Base64UploadDTO implements Serializable {
    @ApiModelProperty("file name")
    private String fileName;

    @ApiModelProperty("file data in base64 encoding")
    private String base64FileData;

    @ApiModelProperty("file group type, for s3 organization")
    private String groupType;

    @ApiModelProperty("file group name, for s3 organization")
    private String groupName;
}
