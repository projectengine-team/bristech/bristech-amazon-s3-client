package br.com.bristech.amazon.s3.client.dto;

import br.com.bristech.amazon.s3.client.entity.File;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Map;

@Getter
@Builder
@AllArgsConstructor
@ApiModel(description = "Upload request specs")
public class UploadRequestDTO implements Serializable {
    @ApiModelProperty("Upload request HTTP method")
    private String method;

    @ApiModelProperty("Upload request HTTP URL")
    private String url;

    @ApiModelProperty("Upload request HTTP headers")
    private Map<String, String> headers;

    @ApiModelProperty("File to be uploaded data")
    private File file;
}
