package br.com.bristech.amazon.s3.client.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Getter
@ToString
@NoArgsConstructor
@ApiModel(description = "File do be upload data")
public class FileUploadDataDTO implements Serializable {
    @ApiModelProperty("Name of the file to be uploaded")
    private String fileName;

    @ApiModelProperty("Length of the file to be uploaded")
    private Long fileLength;

    @ApiModelProperty("SHA256 of the file's content to be uploaded")
    private String fileContentSha256;

    @ApiModelProperty("Application specific group type, useful for organize files")
    private String groupType;

    @ApiModelProperty("Application specific group name, useful for organize files")
    private String groupName;
}
