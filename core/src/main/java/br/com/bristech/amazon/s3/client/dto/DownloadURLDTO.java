package br.com.bristech.amazon.s3.client.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
@ApiModel(description = "DTO for download URL")
public class DownloadURLDTO implements Serializable {
    @ApiModelProperty("file download URL")
    private String url;
}
