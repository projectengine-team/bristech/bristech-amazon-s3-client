package br.com.bristech.amazon.s3.client.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "File download with base64 encoding")
public class FileDownloadDataDTO implements Serializable {
    @ApiModelProperty("File name")
    private String fileName;

    @ApiModelProperty("File data in base64 encoding")
    private String base64FileData;
}
