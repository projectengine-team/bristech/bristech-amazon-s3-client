package br.com.bristech.amazon.s3.client.entity;

import br.com.bristech.msf.entity.CoreEntity;
import lombok.*;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Builder
@ToString
@Entity
@Getter
@Table(name = "BTX_AMZS3_FILE")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class File extends CoreEntity {
    private static final long serialVersionUID = 1L;
    private static final long MILLIS_TIMEOUT = 24 * 60 * 60 * 1000L;

    @Id
    @Column(name = "id")
    private String id = UUID.randomUUID().toString();

    @Column(name = "name")
    private String name;

    @Column(name = "system_owner")
    private String systemOwner;

    @Column(name = "bucket_name")
    private String bucketName;

    @Column(name = "group_type")
    private String groupType;

    @Column(name = "group_name")
    private String groupName;

    @Column(name = "length")
    private long length;

    @Column(name = "created_on")
    private Date createdOn = new Date();

    @Column(name = "timeout_on")
    private Date timeoutOn = new Date(System.currentTimeMillis() + MILLIS_TIMEOUT);

    public File(String name, String systemOwner, String bucketName, String groupType, String groupName, long length) {
        this.name = name;
        this.systemOwner = systemOwner;
        this.bucketName = bucketName;
        this.groupType = groupType;
        this.groupName = groupName;
        this.length = length;
    }

    public void used() {
        this.timeoutOn = null;
    }

    public void unused() {
        this.timeoutOn = new Date(System.currentTimeMillis() + MILLIS_TIMEOUT);
    }
}
