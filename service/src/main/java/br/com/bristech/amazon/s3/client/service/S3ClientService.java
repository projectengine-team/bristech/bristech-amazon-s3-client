package br.com.bristech.amazon.s3.client.service;

import br.com.bristech.amazon.s3.client.dto.FileDownloadDataDTO;
import br.com.bristech.amazon.s3.client.dto.FileUploadDataDTO;
import br.com.bristech.amazon.s3.client.dto.UploadRequestDTO;
import br.com.bristech.amazon.s3.client.entity.File;
import br.com.bristech.amazon.s3.client.repo.FileRepo;
import br.com.bristech.amazon.s3.client.s3.RequestConfiguration;
import br.com.bristech.amazon.s3.client.s3.S3Client;
import br.com.bristech.msf.repository.ObjectNotFoundException;
import br.com.bristech.msf.service.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Slf4j
@Service
public class S3ClientService extends BaseService {

    @Autowired
    private S3Client s3Client;

    @Autowired
    private FileRepo fileRepo;

    @Value("${s3.bucket.name}")
    private String s3BucketName;

    public UploadRequestDTO generateUploadRequest(String systemOwner, FileUploadDataDTO fileData) {
        File file = fileRepo.save(new File(fileData.getFileName(), systemOwner, s3BucketName,
                fileData.getGroupType(), fileData.getGroupName(), fileData.getFileLength()));

        log.debug("Generating Upload request: systemOwner: {}, fileData: {}.", systemOwner, fileData);
        RequestConfiguration config = s3Client.configureUpload(file, fileData.getFileContentSha256());
        log.debug("Upload Data: {}.", config);

        return UploadRequestDTO.builder()
                .method(config.getMethod())
                .url(config.getURL())
                .headers(config.getHeaders())
                .file(file)
                .build();
    }

    public File upload(String systemOwner, String groupType, String groupName, String fileName, byte[] fileData) throws IOException {
        File file = fileRepo.save(new File(fileName, systemOwner, s3BucketName, groupType, groupName, fileData.length));

        log.debug("Uploading file: ");
        log.debug("\tsystemOwner: {}.", systemOwner);
        log.debug("\tgroupType: {}.", groupType);
        log.debug("\tgroupName: {}.", groupName);
        log.debug("\tfileName: {}.", fileName);
        s3Client.upload(file, fileData);
        log.debug("File successfuly uploaded!");

        return file;
    }

    public String downloadURL(String systemOwner, String id) throws ObjectNotFoundException {
        log.debug("Downloading file: ");
        log.debug("\tID: {}.", id);
        log.debug("\tsystemOwner: {}.", systemOwner);
        File file = findFile(systemOwner, id);
        log.debug("Found file: {}.", file);

        return s3Client.configureDownload(file).getURL();
    }

    private File findFile(String systemOwner, String id) throws ObjectNotFoundException {
        return fileRepo.findById(id)
                .filter(file -> file.getSystemOwner().equals(systemOwner))
                .orElseThrow(() -> new ObjectNotFoundException("File not found."));
    }

    public FileDownloadDataDTO downloadBase64(String systemOwner, String id) throws ObjectNotFoundException, IOException {
        File file = findFile(systemOwner, id);

        byte[] fileData = s3Client.download(file);

        return new FileDownloadDataDTO(file.getName(), Base64.getEncoder().encodeToString(fileData));
    }

    public void setAsUsed(String systemOwner, String id) throws ObjectNotFoundException {
        File file = findFile(systemOwner, id);

        file.used();

        fileRepo.save(file);
    }

    public void setAsUnused(String systemOwner, String id) throws ObjectNotFoundException {
        File file = findFile(systemOwner, id);

        file.unused();

        fileRepo.save(file);
    }

    @Scheduled(cron = "0 0 3 * * ?")
    public void scheduleTaskUsingCronExpression() {
        List<File> files = fileRepo.findAllFilesUnused();

        if (!files.isEmpty()) {
            log.info("Found " + files.size() + " unused files.");

            for (File file : files) {
                log.debug("Removing file " + file.getId());

                fileRepo.delete(file);

                try {
                    s3Client.delete(file);

                    log.debug("File " + file.getId() + " removed successfully.");
                } catch (IOException e) {
                    log.warn("File " + file.getId() + " removal failed in S3. Maybe the file wasn't upload.", e);
                }
            }
        } else {
            log.info("Nog unused file found. Will try again in next trigger.");
        }
    }
}
