package br.com.bristech.amazon.s3.client.s3;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.TimeZone;

/**
 * Calculador de credenciais para integração com serviço S3
 */
@Slf4j
@Service
public class S3CredentialsCalculator {
    private static final String SERVICE = "s3";
    private static final String AWS4_REQUEST = "aws4_request";
    public static final String ALGORITHM_NAME = "AWS4-HMAC-SHA256";
    private static final String AWS4 = "AWS4";

    private static final DateFormat datetimeFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmssXX");
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private static final String SHA256_VAZIO = hexEncoded(sha256(""));

    public static final String HOST = "Host";
    public static final String X_AMZ_ALGORITHM = "X-Amz-Algorithm";
    public static final String X_AMZ_CREDENTIAL = "X-Amz-Credential";
    public static final String X_AMZ_EXPIRES = "X-Amz-Expires";
    public static final String DEFAULT_EXPIRES = "86400";
    public static final String X_AMZ_DATE = "X-Amz-Date";
    public static final String X_AMZ_SIGNED_HEADERS = "X-Amz-SignedHeaders";
    public static final String X_AMZ_CONTENT_SHA_256 = "X-Amz-Content-Sha256";
    public static final String X_AMZ_SIGNATURE = "X-Amz-Signature";
    public static final String AUTHORIZATION = "Authorization";
    public static final String DATE = "Date";
    public static final String CONTENT_LENGTH = "Content-Length";

    static {
        datetimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Value("${aws.access.client.id}")
    private String awsAccessClientId;

    @Value("${aws.access.secret.key}")
    private String awsAccessSecretKey;

    public void applyQueryStringCredentials(RequestConfiguration request) {
        request.header(HOST, request.getHost());

        request.queryString(X_AMZ_ALGORITHM, ALGORITHM_NAME);
        request.queryString(X_AMZ_CREDENTIAL, getClientId() + "/" + getCredentialScope(request));
        request.queryString(X_AMZ_EXPIRES, DEFAULT_EXPIRES);
        request.queryString(X_AMZ_DATE, datetimeFormat.format(request.getDate()));
        request.queryString(X_AMZ_SIGNED_HEADERS, getSignedHeaders(request));
        request.queryString(X_AMZ_CONTENT_SHA_256, getSha256Payload(request));

        String canonicalRequest = calculateCanonicalRequest(request, false);

        String stringToSign = prepareStringToSign(request, canonicalRequest);

        byte[] derivedSigningKey = createDerivedSigningKey(request);

        request.queryString(X_AMZ_SIGNATURE, hexEncoded(hmacSha256(derivedSigningKey, stringToSign.getBytes())));
    }

    /**
     * Calcula e aplica a string de autenticacao para consumo da API S3 e demais headers de suporte<br>
     * Conforme doc: https://docs.aws.amazon.com/pt_br/general/latest/gr/signature-version-4.html
     *
     * @param request Configuração da requisição
     * @return string de autenticação (header Authorization)
     */
    public void applyCredentials(RequestConfiguration request) {
        request.header(HOST, request.getHost());
        request.header(X_AMZ_DATE, datetimeFormat.format(request.getDate()));
        request.header(X_AMZ_CONTENT_SHA_256, getSha256Payload(request));

        String canonicalRequest = calculateCanonicalRequest(request);

        String stringToSign = prepareStringToSign(request, canonicalRequest);

        byte[] derivedSigningKey = createDerivedSigningKey(request);

        String authorizationHeader = formatAuthorizationHeader(request, derivedSigningKey, stringToSign.getBytes());

        request.header(AUTHORIZATION, authorizationHeader);
        request.header(DATE, request.getDate().toString());
        if (request.getMethod().equalsIgnoreCase("POST") || request.getMethod().equalsIgnoreCase("PUT")) {
            request.header(CONTENT_LENGTH, Long.toString(request.getPayloadLength()));
        }
    }

    private String getSha256Payload(RequestConfiguration request) {
        return Optional.ofNullable(request.getSha256Payload()).orElse(SHA256_VAZIO);
    }

    private String calculateCanonicalRequest(RequestConfiguration request) {
        return calculateCanonicalRequest(request, true);
    }

    private String calculateCanonicalRequest(RequestConfiguration request, boolean withPayload) {
        return request.getMethod() + "\n" +
                request.getUri() + "\n" +
                request.getQueryString().entrySet().stream()
                        .map(entry -> {
                            try {
                                return entry.getKey() + "=" + URLEncoder.encode(Optional.ofNullable(entry.getValue()).orElse(""), "UTF-8")
                                        .replaceAll("%2D", "-").replaceAll("%7E", "~");
                            } catch (UnsupportedEncodingException e) {
                                return entry.getKey() + "=" + Optional.ofNullable(entry.getValue()).orElse("");
                            }
                        }).sorted()
                        .reduce((qp1, qp2) -> qp1 + "&" + qp2).orElse("") + "\n" +
                request.getHeaders().entrySet().stream().map(entry -> entry.getKey().toLowerCase() + ":" + entry.getValue().trim() + "\n").sorted().reduce((h1, h2) -> h1 + "" + h2).orElse("") + "\n" +
                getSignedHeaders(request) + "\n" +
                (withPayload ? getSha256Payload(request) : "UNSIGNED-PAYLOAD");
    }

    private String getSignedHeaders(RequestConfiguration request) {
        return request.getHeaders().keySet().stream().sorted().map(String::toLowerCase).reduce((k1, k2) -> k1 + ";" + k2).orElse("");
    }

    private byte[] createDerivedSigningKey(RequestConfiguration request) {
        byte[] dateKey = hmacSha256((AWS4 + getSecretKey()).getBytes(), dateFormat.format(request.getDate()).getBytes());
        byte[] dateRegionKey = hmacSha256(dateKey, request.getRegion().getBytes());
        byte[] dateRegionServiceKey = hmacSha256(dateRegionKey, SERVICE.getBytes());
        return hmacSha256(dateRegionServiceKey, AWS4_REQUEST.getBytes());
    }

    private String prepareStringToSign(RequestConfiguration request, String canonicalRequest) {
        return ALGORITHM_NAME + "\n" +
                datetimeFormat.format(request.getDate()) + "\n" +
                getCredentialScope(request) + "\n" +
                hexEncoded(sha256(canonicalRequest));
    }

    private String formatAuthorizationHeader(RequestConfiguration request,
                                             byte[] derivedSigningKey, byte[] stringToSign) {
        String credential = getClientId() + "/" + getCredentialScope(request);
        String signature = hexEncoded(hmacSha256(derivedSigningKey, stringToSign));
        String sinedHeaders = getSignedHeaders(request);

        return ALGORITHM_NAME + " Credential=" + credential +
                ", SignedHeaders=" + sinedHeaders +
                ", Signature=" + signature;

    }

    private String getCredentialScope(RequestConfiguration request) {
        return dateFormat.format(request.getDate()) + "/" + request.getRegion() + "/" + SERVICE + "/" + AWS4_REQUEST;
    }

    private String getClientId() {
        return awsAccessClientId;
    }

    private String getSecretKey() {
        return awsAccessSecretKey;
    }

    private static byte[] sha256(String data) {
        return DigestUtils.sha256(data);
    }

    /**
     * Realiza um HMAC usando {@param key} em {@param data} e retorna um base64 do resultado
     *
     * @param key  chave para assinatura
     * @param data valor a ser assinado
     * @return valor assinado em base64
     */
    private byte[] hmacSha256(byte[] key, byte[] data) {
        return HmacUtils.getInitializedMac(HmacAlgorithms.HMAC_SHA_256, key).doFinal(data);
    }

    /**
     * converte um array de bytes para hexadecimal
     *
     * @param base64 valor em array de bytes
     * @return valor em hexadecimal
     */
    private static String hexEncoded(byte[] base64) {
        return Hex.encodeHexString(base64);
    }

}
