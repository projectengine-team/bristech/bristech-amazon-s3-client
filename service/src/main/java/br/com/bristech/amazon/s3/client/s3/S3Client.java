package br.com.bristech.amazon.s3.client.s3;

import br.com.bristech.amazon.s3.client.entity.File;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.ConnectException;
import java.net.URL;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Slf4j
@Service
public class S3Client {

    private static final String HOST_BASE = "{0}.s3.amazonaws.com";
    private static final DateFormat datetimeFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmssXX");

    private static final int BUFF_SIZE = 100 * 1024;

    static {
        datetimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Autowired
    private S3CredentialsCalculator s3CredentialsCalculator;

    @Value("${s3.region.id}")
    private String s3Region;

    private String getHost(File file) {
        return MessageFormat.format(HOST_BASE, file.getBucketName());
    }

    public void upload(File file, byte[] data) throws IOException {
        execute(configureUpload(
                file,
                DigestUtils.sha256Hex(data)
        ), new ByteArrayInputStream(data));
    }

    public RequestConfiguration configureUpload(File file, String payloadSha256) {
        String host = getHost(file);
        log.debug("Configuring upload to host: {}", host);
        RequestConfiguration request = RequestConfiguration.put(RequestConfiguration.RequestProtocol.HTTPS, host)
                .uri(getObjectURI(file))
                .region(s3Region)
                .payloadLength(file.getLength())
                .sha256Payload(payloadSha256)
                .build();

        log.debug("Request configuration: {}.", request);

        log.debug("Applying query string credentials.");
        s3CredentialsCalculator.applyCredentials(request);
        log.debug("Complete, request: {}", request);

        return request;
    }

    public RequestConfiguration configureDownload(File file) {

        String host = getHost(file);
        log.debug("Configuring download from host: {}", host);

        RequestConfiguration request = RequestConfiguration.get(RequestConfiguration.RequestProtocol.HTTPS, host)
                .uri(getObjectURI(file))
                .region(s3Region)
                .build();

        log.debug("Request: {}", request);

        log.debug("Applying query string credentials.");
        s3CredentialsCalculator.applyQueryStringCredentials(request);
        log.debug("Complete, request: {}", request);

        return request;
    }

    public byte[] download(File file) throws IOException {
        return execute(configureDownload(
                file
        ), null).getBody();
    }


    public RequestConfiguration configureDelete(File file) {
        String host = getHost(file);

        RequestConfiguration request = RequestConfiguration.delete(RequestConfiguration.RequestProtocol.HTTPS, host)
                .uri(getObjectURI(file))
                .region(s3Region)
                .build();

        s3CredentialsCalculator.applyQueryStringCredentials(request);

        return request;
    }

    private String getObjectURI(File file) {
        String baseURI = "/" + file.getSystemOwner() + "/";

        if (file.getGroupType() != null && !file.getGroupType().isEmpty()) {
            baseURI += file.getGroupType() + "/";
        }

        if (file.getGroupName() != null && !file.getGroupName().isEmpty()) {
            baseURI += file.getGroupName() + "/";
        }

        return baseURI + file.getId().substring(0, 2) + "/" +
                file.getId().substring(2, 4) + "/" +
                file.getId().substring(4, 6) + "/" +
                file.getId().substring(6, 8) + "/" +
                file.getId().substring(9) + "/" +
                file.getName();
    }

    public void delete(File file) throws IOException {
        RequestConfiguration configuration = configureDelete(
                file
        );

        execute(configuration, null);
    }

    private ExecuteResponse execute(RequestConfiguration configuration, InputStream payloadInputStream) throws IOException {
        HttpsURLConnection urlConnection = (HttpsURLConnection) new URL(configuration.getURL()).openConnection();

        log.debug("Executing [{}] operation", configuration.getMethod());
        urlConnection.setRequestMethod(configuration.getMethod());

        configuration.getHeaders().forEach(urlConnection::addRequestProperty);
        if (payloadInputStream != null) {
            urlConnection.setDoOutput(true);
            urlConnection.setFixedLengthStreamingMode(configuration.getPayloadLength());
        } else {
            urlConnection.setDoOutput(false);
        }

        log.debug("Connecting to: {}", urlConnection);

        urlConnection.connect();

        if (payloadInputStream != null) {
            log.debug("Payload is not null, transfering to connection stream.");
            transfer(payloadInputStream, urlConnection.getOutputStream());
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int status = 0;
        try {
            status = urlConnection.getResponseCode();
            log.debug("Response status code: {}.", status);

            if (status >= 400) {
                transfer(urlConnection.getErrorStream(), baos);

                throw new ConnectException("Falha na requisição " + configuration.getMethod() + " " +
                        configuration.getURL() + ": " + urlConnection.getResponseCode() +
                        "\n" + new String(baos.toByteArray()));
            }

            log.debug("Transfering request stream to output stream.");
            transfer(urlConnection.getInputStream(), baos);

            log.debug("Sending response.");
            return new ExecuteResponse(urlConnection.getHeaderFields(), baos.toByteArray());
        } catch (Exception ex) {
            try {
                transfer(urlConnection.getErrorStream(), baos);

                throw new IOException("Falha na requisição " + configuration.getMethod() + " " +
                        configuration.getURL() + ": " + status + "\n" + new String(baos.toByteArray()), ex);
            } catch (Exception ex0) {
                throw new IOException("Falha na requisição " + configuration.getMethod() + " " +
                        configuration.getURL(), ex);
            }
        } finally {
            urlConnection.disconnect();
        }
    }

    private void transfer(InputStream is, OutputStream os) throws IOException {
        if (is != null) {
            byte[] buff = new byte[BUFF_SIZE];
            int read;

            while ((read = is.read(buff)) > 0) {
                os.write(buff, 0, read);
            }

            os.flush();
        }
    }

}

@Getter
@Builder
@AllArgsConstructor
class ExecuteResponse {
    private Map<String, List<String>> headers;
    private byte[] body;
}
