package br.com.bristech.amazon.s3.client.repo;

import br.com.bristech.amazon.s3.client.entity.File;
import br.com.bristech.msf.repository.CrudRepo;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FileRepo extends CrudRepo<File, String> {

    @Query("select f from File f where f.timeoutOn <= CURRENT_TIMESTAMP order by timeoutOn")
    List<File> findAllFilesUnused();
}
