package br.com.bristech.amazon.s3.client.s3;

import lombok.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@ToString
@Getter
@Builder
public class RequestConfiguration {
    @NonNull
    private RequestProtocol protocol;
    private long payloadLength;
    @NonNull
    private String host;
    @NonNull
    private String region;
    @NonNull
    private String method;
    private final Date date = new Date();
    private String uri;
    private final Map<String, String> queryString = new HashMap<>();
    private final Map<String, String> headers = new HashMap<>();
    private String sha256Payload;

    public static RequestConfigurationBuilder get(RequestProtocol protocol, String host) {
        return RequestConfiguration.builder().method("GET").protocol(protocol).host(host);
    }

    public static RequestConfigurationBuilder put(RequestProtocol protocol, String host) {
        return RequestConfiguration.builder().method("PUT").protocol(protocol).host(host);
    }

    public static RequestConfigurationBuilder delete(RequestProtocol protocol, String host) {
        return RequestConfiguration.builder().method("DELETE").protocol(protocol).host(host);
    }

    public RequestConfiguration header(String key, String value) {
        headers.put(key, value);
        return this;
    }

    public String getUri() {
        return Optional.ofNullable(uri).orElse("/");
    }

    public String getURL() {
        return getProtocol().getValor() + "://" + getHost() + getUri() + queryString();
    }

    private String queryString() {
        return queryString.isEmpty() ? "" :
                "?" + queryString.entrySet().stream()
                        .map(entry -> entry.getKey() + "=" + Optional.ofNullable(entry.getValue()).orElse(""))
                        .sorted()
                        .reduce((qp1, qp2) -> qp1 + "&" + qp2).orElse("");
    }

    public RequestConfiguration queryString(String chave, String valor) {
        queryString.put(chave, valor);
        return this;
    }

    @Getter
    @RequiredArgsConstructor
    public enum RequestProtocol {
        HTTPS("https"),
        HTTP("http");

        private final String valor;
    }
}
