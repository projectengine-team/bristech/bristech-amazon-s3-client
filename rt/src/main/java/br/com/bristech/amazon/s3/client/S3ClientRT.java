package br.com.bristech.amazon.s3.client;

import jakarta.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@ComponentScan({ "br.com.bristech.amazon.s3.client" })
@EntityScan(basePackages = { "br.com.bristech.amazon.s3.client" })
@EnableJpaRepositories(basePackages = { "br.com.bristech.amazon.s3.client" })
public class S3ClientRT {

	@Autowired
	private Environment environment;

	public static void main(String[] args) {
		SpringApplication.run(S3ClientRT.class, args);
	}

	@PostConstruct
	public void init() {
		log.info("br.com.bristech.amazon.s3.client Iniciado.");
		for (String profileName : environment.getActiveProfiles()) {
			log.info("Ambiente de execução: {}", profileName);
		}

	}
}
