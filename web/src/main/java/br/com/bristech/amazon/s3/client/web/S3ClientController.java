package br.com.bristech.amazon.s3.client.web;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.postgresql.shaded.com.ongres.scram.common.bouncycastle.base64.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.bristech.amazon.s3.client.dto.Base64UploadDTO;
import br.com.bristech.amazon.s3.client.dto.DownloadURLDTO;
import br.com.bristech.amazon.s3.client.dto.FileDownloadDataDTO;
import br.com.bristech.amazon.s3.client.dto.FileUploadDataDTO;
import br.com.bristech.amazon.s3.client.dto.UploadRequestDTO;
import br.com.bristech.amazon.s3.client.entity.File;
import br.com.bristech.amazon.s3.client.service.S3ClientService;
import br.com.bristech.msf.repository.ObjectNotFoundException;
import br.com.bristech.web.BaseController;
import br.com.bristech.web.BusinessException;
import br.com.bristech.web.NotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/s3-client")
@Api("Amazon S3 client library")
public class S3ClientController extends BaseController {

	@Autowired
	private S3ClientService service;

	@RequestMapping(value = "/upload/{systemOwner}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Generates a upload request specs for the specified file data.")
	public UploadRequestDTO generateUploadRequest(
			@ApiParam("System owner identifier") @PathVariable("systemOwner") String systemOwner,
			@ApiParam("File to be uploaded data") @RequestBody FileUploadDataDTO fileData) {
		return service.generateUploadRequest(systemOwner, fileData);
	}

	@RequestMapping(value = "/upload/{systemOwner}/multipart", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Uploads a file throw multipart request.")
	public File uploadMultipart(@ApiParam("System owner identifier") @PathVariable("systemOwner") String systemOwner,
			@ApiParam("File to be uploaded data") @RequestParam("groupType") String groupType,
			@ApiParam("File to be uploaded data") @RequestParam("groupName") String groupName,
			@ApiParam("File to be uploaded data") @RequestPart("file") MultipartFile file) {
		try {
			return service.upload(systemOwner, groupType, groupName, file.getName(), file.getBytes());
		} catch (IOException e) {
			throw new BusinessException("Couldn't parse the file data.", e);
		}
	}

	@RequestMapping(value = "/upload/{systemOwner}/base64", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Uploads a file encoded in base 64.")
	@ApiResponses({ @ApiResponse(code = 400, message = "Couldn't create URL for redirecting"), })
	public File uploadBase64(@ApiParam("System owner identifier") @PathVariable("systemOwner") String systemOwner,
			@ApiParam("File to be uploaded data") @RequestBody Base64UploadDTO fileData) {
		try {
			return service.upload(systemOwner, fileData.getFileName(), fileData.getGroupType(), fileData.getGroupName(),
					Base64.decode(fileData.getBase64FileData()));
		} catch (IOException e) {
			throw new BusinessException("Couldn't upload the file", e);
		}
	}

	@RequestMapping(value = "/download/{systemOwner}/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Download the file speficied by redirecting to a S3 download URL.")
	@ApiResponses({ @ApiResponse(code = 404, message = "File not found with the specified systemOwner and id"),
			@ApiResponse(code = 400, message = "Couldn't create URL for redirecting"), })
	public ResponseEntity<?> download(
			@ApiParam("System owner identifier") @PathVariable("systemOwner") String systemOwner,
			@ApiParam("File identifier") @PathVariable("id") String id) {
		try {
			String url = service.downloadURL(systemOwner, id);

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setLocation(new URI(url));
			return new ResponseEntity<>(httpHeaders, HttpStatus.FOUND);
		} catch (ObjectNotFoundException ex) {
			throw new NotFoundException("File not found.");
		} catch (URISyntaxException e) {
			throw new BusinessException("Couldn't create URL for redirecting", e);
		}
	}

	@RequestMapping(value = "/download/{systemOwner}/{id}/url", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Returns the specified file URL for downloading directly via S3.")
	@ApiResponses({ @ApiResponse(code = 404, message = "File not found with the specified systemOwner and id"),
			@ApiResponse(code = 400, message = "Couldn't create URL for redirecting"), })
	public DownloadURLDTO downloadURL(
			@ApiParam("System owner identifier") @PathVariable("systemOwner") String systemOwner,
			@ApiParam("File identifier") @PathVariable("id") String id) {
		try {
			return new DownloadURLDTO(service.downloadURL(systemOwner, id));
		} catch (ObjectNotFoundException ex) {
			throw new NotFoundException("File not found.");
		}
	}

	@RequestMapping(value = "/download/{systemOwner}/{id}/base64", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Download the file speficied throw base64 encoded data.")
	@ApiResponses({ @ApiResponse(code = 404, message = "File not found with the specified systemOwner and id"),
			@ApiResponse(code = 400, message = "Couldn't download file"), })
	public FileDownloadDataDTO downloadBase64(
			@ApiParam("System owner identifier") @PathVariable("systemOwner") String systemOwner,
			@ApiParam("File identifier") @PathVariable("id") String id) {
		try {
			return service.downloadBase64(systemOwner, id);
		} catch (ObjectNotFoundException ex) {
			throw new NotFoundException("File not found.");
		} catch (IOException e) {
			throw new BusinessException("Couldn't download file", e);
		}
	}

	@RequestMapping(value = "/used/{systemOwner}/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Defines the specified file as used.")
	@ApiResponses({ @ApiResponse(code = 404, message = "File not found with the specified systemOwner and id"),
			@ApiResponse(code = 400, message = "Couldn't download file"), })
	public void setAsUsed(@ApiParam("System owner identifier") @PathVariable("systemOwner") String systemOwner,
			@ApiParam("File identifier") @PathVariable("id") String id) {
		try {
			service.setAsUsed(systemOwner, id);
		} catch (ObjectNotFoundException ex) {
			throw new NotFoundException("File not found.");
		}
	}

	@RequestMapping(value = "/unused/{systemOwner}/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Defines the specified file as unused.")
	@ApiResponses({ @ApiResponse(code = 404, message = "File not found with the specified systemOwner and id"),
			@ApiResponse(code = 400, message = "Couldn't download file"), })
	public void setAsUnused(@ApiParam("System owner identifier") @PathVariable("systemOwner") String systemOwner,
			@ApiParam("File identifier") @PathVariable("id") String id) {
		try {
			service.setAsUnused(systemOwner, id);
		} catch (ObjectNotFoundException ex) {
			throw new NotFoundException("File not found.");
		}
	}
}
